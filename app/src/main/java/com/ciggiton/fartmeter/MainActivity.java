package com.ciggiton.fartmeter;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaRecorder;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.WebDialog;
import com.startapp.android.publish.StartAppSDK;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.startapp.android.publish.StartAppSDK;
import com.startapp.android.publish.banner.Banner;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    protected Button mBnProcessFart;
    protected TextView mTxtFartability;
    protected double mFartability = 0.0;
    //
    //private CallbackManager callbackManager;
    //private LoginManager loginManager;
    Facebook fb;
    //private UiLifecycleHelper uiHelper;

    /*
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            //onSessionStateChange(session, state, exception);
        }
    };
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StartAppSDK.init(this, getString(R.string.startapp_id), true);
        setContentView(R.layout.activity_main);
        fb = new Facebook(getString(R.string.app_id));
        initControls();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        fb.authorizeCallback(requestCode, responseCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (fb.isSessionValid()) {
            //button close our session - log out of facebook
            try {
                if(getApplicationContext() != null) {
                    fb.logout(getApplicationContext());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bnProcessFart) {
            measureFartability();
        } else {
        }
    }

    private void sharePhotoToFacebook(){
        String response = null;
        try {
            response = fb.request("me");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bundle parameters = new Bundle();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.com_facebook_profile_default_icon);
        byte[] data;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        data = baos.toByteArray();

        parameters.putString("message", "test share image");

        parameters.putString("description", "test test test");

        parameters.putByteArray("picture", data);
        try {
            response = fb.request("me/photos/feed", parameters, "POST");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sharePhotoToFacebook1(){
        String response = null;
        try {
            response = fb.request("me");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bundle parameters = new Bundle();
        View v = findViewById(R.id.rlScreen).getRootView();
        v.setDrawingCacheEnabled(true);
        Bitmap bitmap = v.getDrawingCache();
        byte[] data;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        data = baos.toByteArray();

        parameters.putString("message", "My fartability...");

        parameters.putString("description", "This is my fartability result");

        parameters.putByteArray("picture", data);
        try {
            response = fb.request("me/photos/feed", parameters, "POST");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initControls(){
        Typeface myTypeface = Typeface.createFromAsset(this.getAssets(), "digital-7.ttf");

        mBnProcessFart = (Button)findViewById(R.id.bnProcessFart);
        mTxtFartability = (TextView)findViewById(R.id.txtFartability);
        mTxtFartability.setTypeface(myTypeface);

        mBnProcessFart.setOnClickListener(this);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public MainActivity getMainActivity(){
        return this;
    }

    protected void showCustomDialog(String message) {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_dialogbox);

        final TextView txtDialogMessage = (TextView)dialog.findViewById(R.id.txtDialogMessage);
        txtDialogMessage.setText(message);

        Button bnYes = (Button)dialog.findViewById(R.id.bnYes);
        bnYes.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                //login to facebook
                fb.authorize(MainActivity.this, new Facebook.DialogListener() {
                    //fb.authorize(MainActivity.this, new String[]{"email"}, new DialogListener() {
                    @Override
                    public void onComplete(Bundle values) {
                        //share screenshot here
                        sharePhotoToFacebook1();
                    }

                    @Override
                    public void onFacebookError(FacebookError e) {
                        Toast.makeText(MainActivity.this, "onFacebookError", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(DialogError e) {
                        Toast.makeText(MainActivity.this, "onError", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(MainActivity.this, "onCancel", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.dismiss();
            }
        });

        Button bnNoThanks = (Button)dialog.findViewById(R.id.bnNoThanks);
        bnNoThanks.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void measureFartability(){
        mFartability = 0.0;
        mBnProcessFart.setEnabled(false);

        MediaRecorder mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        Timer mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new RecorderTask(mRecorder, mTimer, 100), 0, 100);
        mRecorder.setOutputFile("/dev/null");

        try {
            mRecorder.prepare();
            mRecorder.start();
        } catch(IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Recorder Task
    private class RecorderTask extends TimerTask {
        TextView sound = (TextView) findViewById(R.id.txtFartability);
        MediaRecorder mRecorder;
        Timer mTimerRef;
        int mCounter;

        public RecorderTask(MediaRecorder recorder, Timer timerRef, int count) {
            this.mRecorder = recorder;
            this.mTimerRef = timerRef;
            this.mCounter = count;
        }

        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (mCounter <= 0) {
                        mTimerRef.cancel();

                        /*
                        mRecorder.stop();
                        mRecorder.release();
                        mRecorder = null;
                        */
                        //
                        try {
                            mRecorder.stop();
                        } catch(RuntimeException e) {
                            //mFile.delete();  //you must delete the outputfile when the recorder stop failed.
                        } finally {
                            mRecorder.release();
                            mRecorder = null;
                        }
                        //
                        getMainActivity().mBnProcessFart.setEnabled(true);

                        getMainActivity().showCustomDialog(
                                "Your Fartability is: " + round(getMainActivity().mFartability, 2) + "\n" +
                                "Do you want to share this on facebook?"
                        );
                    }

                    if(mRecorder != null) {
                        int amplitude = mRecorder.getMaxAmplitude();
                        double amplitudeDb = (amplitude == 0) ? 0.0 : 20 * Math.log10((double) Math.abs(amplitude));
                        //double amplitudeDb = (amplitude == 0) ? 0.0 : 20 * Math.log10((double) Math.abs(amplitude/0.0000001));
                        //sound.setText("" + round(amplitudeDb, 2));
                        getMainActivity().mFartability += amplitudeDb;
                        sound.setText("" + round(getMainActivity().mFartability, 2));
                    }
                    --mCounter;
                }
            });
        }
    }
}


